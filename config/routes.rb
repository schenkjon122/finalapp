Rails.application.routes.draw do
  get 'sessions/new'

  root 'static#home'
  get 'about' => 'static#about'
  get 'contact' => 'static#contact'
  get 'signup' => 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  resources :users
end
